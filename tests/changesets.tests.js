var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;
var node = require('./helper').changeset;
var comment = {content: 'A new comment for the changeset'};

describe('Repository', function() {
  var repo;

  before(function (done) {
    var client = bitbucket.createClient(goodOptions);
    client.getRepository(repoData, function (err, repository) {
      if (err) {return done(err);}
      repo = repository;
      done();
    });
  });

  // describe(".changesets()", function () {
  //   describe(".get()", function () {
  //     it('should return a list of changeset', function (done) {
  //       var limit = 15;
  //       repo.changesets().get(limit, node, function (err, result) {
  //         result.start.should.eql(node);
  //         result.limit.should.eql(limit);
  //         done(err);
  //       });
  //     });
  //   });

  //   describe(".getById()", function () {
  //     it('should return a given changeset', function (done) {
  //       repo.changesets().getById(node, function (err, result) {
  //         result.message.should.eql('Initial commit and setup.\n');
  //         done(err);
  //       });
  //     });
  //   });
  // });


  // describe(".changeset(node)", function () {
  //   describe(".getStats()", function () {
  //     it ('should return the stats of the given changeset', function (done) {
  //       repo.changeset(node).getStats(function (err, result) {
  //         Array.isArray(result).should.be.ok;
  //         if (result.length > 0) {
  //           result[0].should.have.property('type');
  //           result[0].should.have.property('file');
  //           result[0].should.have.property('diffstat');
  //         }
  //         done(err);
  //       });
  //     });
  //   });

    // describe(".getLikes()", function () {
    //   it ('should return the likes for a given changeset', function (done) {
    //     repo.changeset(node).getLikes(function (err, result) {
    //       Array.isArray(result).should.be.ok;
    //       done(err);
    //     });
    //   });
    // });

    // describe(".getDiff()", function () {
    //   it ('should return the diff for a given changeset', function (done) {
    //     repo.changeset(node).getLikes(function (err, result) {
    //       Array.isArray(result).should.be.ok;
    //       done(err);
    //     });
    //   });
    // });

    // describe(".changeset(node).comments()", function () {
    //   var commentId;
      // describe(".create()", function () {
      //   it('should create a new comment', function (done) {
      //     repo.changeset(node).comments().create(comment, function (err, result) {
      //       commentId = result.comment_id;
      //       result.content.should.eql(comment.content);
      //       done(err);
      //     });
      //   });
      // });

      // describe(".getAll()", function () {
      //   it('should get all comments for the issue', function (done) {
      //     repo.changeset(node).comments().getAll(function (err, result) {
      //       Array.isArray(result).should.be.ok;
      //       done(err);
      //     });
      //   });
      // });

      // describe(".update()", function () {
      //   it('should update a comment', function (done) {
      //     repo.changeset(node).comments().update(commentId, {content: "Updated content"}, function (err, result) {
      //       result.content.should.eql("Updated content");
      //       done(err);
      //     });
      //   });
      // });

      // describe(".remove()", function () {
      //   it('should remove a given comment', function (done) {
      //     repo.changeset(node).comments().remove(commentId, function (err, result) {
      //       result.success.should.be.ok;
      //       done(err);
      //     });
      //   });
      // });
  //   });
  // });

});
