var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;
var issue = {title: "New Issue", content: "Issue content"};
var comment = 'This is the content of the issue. Via the api.';

describe('Repository', function() {
  var repo;

  before(function (done) {
    var client = bitbucket.createClient(goodOptions);
    client.getRepository(repoData, function (err, repository) {
      if (err) {return done(err);}
      repo = repository;
      done();
    });
  });

  describe(".issue(id).comments()", function () {
    var issueId = null;
    var commentId = null;
    before(function (done) {
      repo.issues().create(issue, function (err, result) {
        issueId = result.local_id;
        done(err);
      });
    });

    describe(".create()", function () {
      it('should create a new comment', function (done) {
        repo.issue(issueId).comments().create(comment, function (err, result) {
          commentId = result.comment_id;
          result.content.should.eql(comment);
          done(err);
        });
      });
    });

    describe(".getById()", function () {
      it('should get the comment by the id', function (done) {
        repo.issue(issueId).comments().getById(commentId, function (err, result) {
          result.content.should.eql(comment);
          result.comment_id.should.eql(commentId);
          done(err);
        });
      });
    });

    describe(".getAll()", function () {
      it('should get all comments for the issue', function (done) {
        repo.issue(issueId).comments().getAll(function (err, result) {
          Array.isArray(result).should.be.ok;
          done(err);
        });
      });
    });

    describe(".update()", function () {
      it('should update a comment', function (done) {
        repo.issue(issueId).comments().update(commentId, "Updated content", function (err, result) {
          result.content.should.eql("Updated content");
          done(err);
        });
      });
    });

    describe(".remove()", function () {
      it('should remove a given comment', function (done) {
        repo.issue(issueId).comments().remove(commentId, function (err, result) {
          (result === null).should.be.ok;
          done(err);
        });
      });
    });

    after(function (done) {
      repo.issues().remove(issueId, function (err, result) {
        done(err);
      });
    });
  });

  describe(".issues()", function () {
    describe(".get()", function () {
      it('should return a list of issues', function (done) {
        repo.issues().get(function (err, issues) {
          issues.should.have.property('issues');
          issues.should.have.property('count');
          done();
        });
      });
    });

    describe(".create()", function () {
      it('should create a new issue', function (done) {
        repo.issues().create(issue, function (err, result) {
          result.status.should.eql("new");
          result.title.should.eql(issue.title);
          done(err);
        });
      });
    });

    describe(".update()", function () {
      it('should update an issue', function (done) {
        repo.issues().get(function (err, issues) {
          var issue = issues.issues[0];
          repo.issues().update(issue.local_id, {title: 'Not new anymore'}, function (err, result) {
            result.title.should.eql('Not new anymore')
            done(err);
          });
        });
      });
    });

    describe(".remove()", function () {
      it('should remove an issue', function (done) {
        repo.issues().get(function (err, issues) {
          repo.issues().remove(issues.issues[0].local_id, function (err, result) {
            (result === null).should.be.ok;
            done(err);
          });
        });
      });
    });

    after(function(done) {
      repo.issues().get(function (err, issues) {
        if (issues.issues.length === 0) { return done(); }
        issues.issues.forEach(function (issue) {
          repo.issues().remove(issue.local_id, function (err, result) {
            done(err);
          });
        })
      });
    });
  });

});
